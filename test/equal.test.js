test("test toBe",() => {
    const name = "hilman";
    const hello = `Hello ${name}`

    expect(hello).toBe('Hello hilman');
});

test("test object",()=>{
    let person = {
        id :"1"
    }
    Object.assign(person,{
        name:"hilman"
    });

    expect(person).toEqual({
        id:"1",
        name:"hilman"
    })
})