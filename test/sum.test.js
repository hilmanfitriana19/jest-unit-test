import { sum } from "../src/sum.js";

test("sum(1,2) must be 3",() => {
    var result = sum(1,2)

    expect(result).toBe(3);
})

test("sum(1,3) must be 4",() => {
    var result = sum(1,3)

    expect(result).toBe(4);
})

test("sum(1,4) must be 5",() => {
    var result = sum(1,4  )

    expect(result).toBe(5);
})